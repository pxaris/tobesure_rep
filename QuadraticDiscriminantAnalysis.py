
import numpy as np
from scipy.stats import multivariate_normal
import csv
 
def loadDataset(filename, fold, no_folds, trainingSet=[] , testSet=[]):
	with open(filename, 'r') as csvfile:
	    lines = csv.reader(csvfile)
	    dataset = list(lines)
	    for x in range(len(dataset)):
	        for y in range(len(dataset[x])):
	            dataset[x][y] = float(dataset[x][y])
	        if ((x >= fold*(len(dataset)-1)/no_folds) and (x <= (fold+1)*(len(dataset)-1)/no_folds)) : #separates the two sets, 1/10 to the test set
	            testSet.append(dataset[x])
	        else:
	            trainingSet.append(dataset[x])
	    print ('Total patterns at Train set: ' + repr(len(trainingSet)))
	    print ('Test set from pattern: ' + repr((fold*(len(dataset))//no_folds)+1) + ' to: ' + repr((fold+1)*(len(dataset))//no_folds))



def separateByClass(dataset):
	separated = {}
	for i in range(len(dataset)):
		vector = dataset[i]
		if (vector[-1] not in separated):    #vector[-1] is the last attribute, the class
			separated[vector[-1]] = []
		separated[vector[-1]].append(vector[:-1])
	return separated                #map of each class value to a list of instances that belong to that class




def summarizeByClass(dataset): #meanVector and covarianceMatrix by Class
	separated = separateByClass(dataset)
	summaries = {}
	for classValue, instances in separated.items():  #separated is a map
		meanVector = np.mean(instances, axis=0)
		classVectors = np.array(instances).T
		covMatrix = np.cov(classVectors)
		summaries[classValue] = [meanVector, covMatrix]
	return summaries


def classProbability(dataset): # calculates p(c) here
	separated = separateByClass (dataset)
	classProb = {}
	for classValue, instances in separated.items():
		classProb[classValue] = len(instances)/len(dataset)
	return classProb


def calculateProbability(x, mean, covMatrix):
	var = multivariate_normal(mean = mean , cov = covMatrix)
	return var.pdf(x)  #assumes  multivariate Gaussian pdf for the class


def calculateClassProbabilities(summaries, x, trainingSet):
	probabilities = {}
	classProb = classProbability(trainingSet)
	for classValue, classSummaries in summaries.items():  #summaries is a map
		meanVector = classSummaries[0]
		covMatrix = classSummaries[1]
		probabilities[classValue] = calculateProbability(x, meanVector, covMatrix)
		probabilities[classValue] *= classProb[classValue]
	return probabilities


def predict(summaries, inputVector, trainingSet):
	probabilities = calculateClassProbabilities(summaries, inputVector, trainingSet)
	bestLabel, bestProb = None, -1
	for classValue, probability in probabilities.items():
		if bestLabel is None or probability > bestProb:
			bestProb = probability
			bestLabel = classValue
	return bestLabel


def getAccuracy(testSet, predictions):
	correct = 0
	for i in range(len(testSet)):
		if testSet[i][-1] == predictions[i]:
			correct += 1
	return (correct/float(len(testSet))) * 100.0


def main():

	filename = 'pima-indians-diabetes.data' # uncomment for pima indians diabetes problem
	no_folds = 10 # for 10-fold cross validation

	listAccuracy = []
	
	for fold in range(0 , no_folds):
		print ('Iteration: ' + repr(fold+1))
		trainingSet=[]
		testSet=[]
		loadDataset(filename, fold, no_folds, trainingSet, testSet)
		predictions=[]
		summaries = summarizeByClass(trainingSet)

		for x in range(len(testSet)):
			testVector = testSet[x]
			result = predict(summaries, testVector[:-1], trainingSet)
			predictions.append(result)
			#print('> predicted=' + repr(result) + ', actual=' + repr(testSet[x][-1]))  #prints prediction and actual class for every pattern in the test set
		
		accuracy = getAccuracy(testSet, predictions)
		
		#print (repr(testSet))           #you can print the two sets with those commands for every iteration
		#print (repr(trainingSet))
		
		print ('Accuracy: ' + repr(accuracy))
		print ('')
		listAccuracy.append(accuracy) 
	
	print('List of Accuracies: ' + repr(listAccuracy) + '%')
	meanAccuracy = sum(listAccuracy) / float(len(listAccuracy))
	print('Average Accuracy: ' + repr(meanAccuracy))

main()
